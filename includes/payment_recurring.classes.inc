<?php

/**
 * Classes for Payment Recurring.
 */

/**
 * Class PaymentRecurringEntityController
 *
 * Extends PaymentEntityController to bundle recurring payments with the same
 * origin.
 */
class PaymentRecurringEntityController extends PaymentEntityController {

  /**
   * {@inheritdoc}
   */
  function attachLoad(&$queried_entities, $revision_id = FALSE) {
    $pids = array_keys($queried_entities);

    // Load payment bundles.
    $result = db_select('payment_recurring_payments')
      ->fields('payment_recurring_payments')
      ->condition('pid', $pids)
      ->execute();
    while ($first_payment_data = $result->fetchObject()) {
      // Differentiate normal payments from the first payments of recurring payments.
      if (!isset($queried_entities[$first_payment_data->pid]->recurring)) {
        $queried_entities[$first_payment_data->pid]->recurring = array();
      }

      if ($first_payment_data->pid != $first_payment_data->fpid) {
        // Add the first payment of a recurring payment.
        $payment = entity_load_single('payment', $first_payment_data->fpid);
        $queried_entities[$first_payment_data->pid]->recurring['first_payment'] = $payment;
      }
    }

    parent::attachLoad($queried_entities, $revision_id);
  }

  /**
   * {@inheritdoc}
   */
  function save($entity, DatabaseTransaction $transaction = NULL) {
    $payment = $entity;

    if (isset($payment->recurring)
      && !isset($payment->recurring['fpid'])
      && !isset($payment->context_data['recurring'])) {
      // For the first payment store the recurring information in the context_data.
      $payment->context_data['recurring'] = $payment->recurring;
      $payment->recurring = array();
    }

    // First save the $payment to get a pid.
    $return = parent::save($payment, $transaction);

    // Save a recurring payment in a bundle.
    if (isset($payment->recurring)) {
      $pid = $payment->pid;
      if (isset($payment->recurring['fpid'])) {
        // We know the first payment.
        $fpid = $payment->recurring['fpid'];

        // Add the first payment to the recurring info.
        $first_payment = entity_load_single('payment', $payment->recurring['fpid']);
        $payment->recurring['first_payment'] = $first_payment;
      }
      else {
        // We assume that this payment is the a first payment itself.
        $fpid = $payment->pid;
      }

      db_merge('payment_recurring_payments')
        ->fields(array(
          'fpid' => $fpid,
        ))
        ->key(array(
          'pid' => $pid,
        ))
        ->execute();
    }

    return $return;
  }
}
