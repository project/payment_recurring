<?php

/**
 * Form callbacks.
 */

/**
 * Operation confirmation form callback.
 *
 * @param $form
 * @param $form_state
 * @param $op
 * @param \Payment $payment
 * @return mixed
 */
function payment_recurring_recurring_payment_operation_form($form, &$form_state, $op, Payment $payment) {
  $form_state['op'] = $op;
  $form_state['payment'] = $payment;

  $recurring_info = payment_recurring_recurring_payment_info($payment);
  $question = t('Are you sure that you want to ' . $op . ' the recurring payment for @name?',
    array('@name' => $recurring_info['name']));
  $form = confirm_form($form, $question, 'payment/' . $payment->pid);

  return $form;
}

function payment_recurring_recurring_payment_operation_form_submit(&$form, &$form_state) {
  /** @var Payment $payment */
  $payment = $form_state['payment'];
  $op = $form_state['op'];

  $result = module_invoke($payment->method->module, 'payment_recurring_' . $op, $payment);
  drupal_goto('payment/' . $payment->pid);
}
