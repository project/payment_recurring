Payment Recurring
=================
Payment Recurring adds support for recurring payments to the Payment platform for Drupal (https://www.drupal.org/project/payment).

Requirements
------------
This module requires the following modules
 - Payment (https://www.drupal.org/project/payment)
 
To be able to process recurring payments you need to install or create at least one payment context and at least one
payment method that supports recurring payments (see Developer Information).

Installation
------------
Install as you would normally install a contributed drupal module. See
https://www.drupal.org/documentation/install/modules-themes/modules-7 for further information.

User Information
----------------
The Payment Recurring module is inteded for develops and does not have a user interface or other functionality
for end users.

Developer Information
---------------------

### Payment contexts
To create recurring payments a payment context should include a 'recurring' key in the payment data when creating a
Payment object.

    $payment_data = array(
      // Your usual payment data first
      ...
      'recurring' => array(
        'type' => 'subscription', // one of (manual, subscription, installments)
        'name' => 'Name of Customer',
        'email' => 'customer@domain.com',
        'interval' => '1 week', // optional, only applicable for types subscription and installments
        'times' => 4, // optional, only applicable for type installments
        'startDate' => '2017-03-24', //optional, in format yyyy-mm-dd
      ),
    );
    
    $first_payment = new Payment($payment_data);

After the payment is saved this information is stored in the context_data property of the Payment object. This creates
a first payment for this recurring payment. The following types of recurring payments are supported at this moment:
 - manual: additional payments can be added manually
 - subscription: additional payments are created automatically every 'interval' until the subscription is ended
 - installments: additional payments are created automatically every 'interval' until 'times' payments have been paid

To load all payments that belong to the same recurring payment use

    $payments = payment_recurring_recurring_payment_load(Payment $payment);

where $payment is the first payment. This returns an array of Payment objects.

To retrieve the recurring information from a Payment object use

    $recurring_info = payment_recurring_recurring_payment_info(Payment $payment);

It returns the array as defined under the 'recurring' key in the payment data from which the first payment was created.
This works for both a first payment and other recurring payments.

### Payment methods
A payment method supporting recurring payments of types subscription or installments will probably call a webhook to
report new payments for the recurring payment. The webhook should create a new Payment object each time a new payment is
reported. The payment should be linked to the first payment by setting the 'recurring' key in the payment data.

    $fpid = $first_payment->pid;
    
    $payment_data = array(
      // Your usual payment data first
      ...
      'recurring' => array(
        'fpid' => $fpid, // pid of the first payment
      ),
    );
    
    $recurring_payment = new Payment($payment_data);